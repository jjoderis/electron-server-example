const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const webpack = require('webpack');

module.exports = {
  mode: 'development',
  entry: {
    index: './src/shared/frontend/index.js',
  },
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      }
    ]
  },
  target: (process.env.API === 'server') ? 'web' : 'electron-renderer',
  plugins: [
    new VueLoaderPlugin(),
    new webpack.NormalModuleReplacementPlugin(/\.\/ms-api-API.js/, result => {
      if (result.request) {
        result.request = result.request.replace(
          'API',
          (process.env.API === 'server') ? 'server' : 'electron',
        )
      }
    }),
  ]
};