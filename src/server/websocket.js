const WebSocket = require('ws');
const { handleMessageEvent, setOnMessageCallback } = require('../shared/backend/userInputHandler.js');

const ws = new WebSocket.Server({ port: 3001 });

let channels = [];

ws.on('connection', (channel) => {
  console.log('New communication channel opened');
  channels.push(channel);
  
  channel.on('message', (data) => {
    /**
     * To send different commands we might want to use messages like:  (command:content)
     */
    const commandEndIndex = data.indexOf(':');
    const command = data.slice(0, commandEndIndex);
    const content = data.slice(commandEndIndex+1);

    switch(command) {
      case 'newMessage':
        handleMessageEvent(content);
        break;
      default:
        console.log('Unknown command received');
        break;
    }
  });

  channel.on('close', () => {
    const index = channels.findIndex((storedChannel) => storedChannel === channel);

    channels = channels.slice(0, index).concat(channels.slice(index+1));
  });
});

setOnMessageCallback((msg) => {
  channels.forEach((channel) => {
    channel.send(msg);
  });
});


module.exports = ws;