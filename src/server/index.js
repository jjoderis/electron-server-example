const express = require('express');
const fs = require('fs');
const path = require('path');
const ws = require('./websocket.js');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  fs.readFile(path.resolve(__dirname, '..', '..', 'dist', 'index.html'), (err, data) => {
    res.setHeader('Content-Type', 'text/html')
    res.statusCode = 200;
    res.send(data);
  });
})

app.get('/:filename.js$', (req, res) => {
  fs.readFile(path.resolve(__dirname, '..', '..',  'dist', `${req.params.filename}.js`), (err, data) => {
    res.setHeader('Content-Type', 'text/javascript')
    res.statusCode = 200;
    res.send(data);
  });
});

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));