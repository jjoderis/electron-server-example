const { app, BrowserWindow, ipcMain } = require('electron');

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let win;

function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    }
  })

  // and load the index.html of the app.
  win.loadFile('../../dist/index.html')
}

app.whenReady().then(createWindow);

function getUserInput() {
  rl.question("Send message to frontend: ", (message) => {
    win.webContents.send('newMessage', { msg: message });
    getUserInput();
  });
}

ipcMain.on('newMessage', (event, { msg }) => {
  console.log(`
Message from frontend: ${msg}
Send message to frontend: `);
});

setTimeout(getUserInput, 500); 

app.on('quit', () => {
  rl.close();
});

