// info source: https://javascript.info/websocket
let channel = null;
let channelOpen = false;

export const openChannel = () => {
  channel = new WebSocket('ws://localhost:3001');

  channel.onmessage = ({ data: msg }) => {
    if (onNewMessageCallback) {
      onNewMessageCallback(msg);
    }
  }
  
  channel.onopen = (event) => {
    channelOpen = true;
    console.log('Communication channel with server open');
  }
  
  channel.onclose = (event) => {
    channelOpen = false;
    console.log('Communication channel to server was closed');
  }
  
  channel.onerror = (error) => {
    console.log('There was an error with the channel');
    console.error(error);
  }
}

let onNewMessageCallback = null;

export const setNewMessageCallback = (cb) => {
  onNewMessageCallback = cb;
}

export const sendCommand = (command, msg) => {
  if (channelOpen) {
    channel.send(`${command}:${msg}`);
  }
}

process.on('beforeExit', () => {
  if (channel) {
    channel.close();
  }
});