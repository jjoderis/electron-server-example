import { ipcRenderer } from 'electron';

export const openChannel = () => {
  console.log('Communication channel with electron open');
}

let onNewMessageCallback = null;

export const setNewMessageCallback = (cb) => {
  onNewMessageCallback = cb;
}

ipcRenderer.on('newMessage', (event, { msg }) => {
  if (onNewMessageCallback) {
    onNewMessageCallback(msg);
  }
});

export const sendCommand = (command, msg) => {
  ipcRenderer.send(command, { msg });
}

