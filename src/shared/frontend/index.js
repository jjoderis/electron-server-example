import Vue from 'vue';
import App from './App.vue';

import { openChannel } from './ms-api/ms-api-interface.js';

openChannel();

const app = new Vue({
  render: (h) => h(App),
}).$mount('#app');