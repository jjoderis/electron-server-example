const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let onMessageCallback = null;

function getUserInput() {
  rl.question("Send message to frontend: ", (message) => {
    if (onMessageCallback) {
      onMessageCallback(message);
    } 
    getUserInput();
  });
}

function handleMessageEvent(msg) {
  console.log(msg);
}

function setOnMessageCallback(cb) {
  onMessageCallback = cb;
}

getUserInput();

module.exports = { handleMessageEvent, setOnMessageCallback };